<?php 
    require_once("config.php"); 
    $res_country = $db->query("SELECT * FROM country");
    $jml_country = $res_country->rowCount();
    $res_city = $db->query("SELECT * FROM city");
    $jml_city = $res_city->rowCount();
    $res_cl = $db->query("SELECT * FROM countrylanguage");
    $jml_cl = $res_cl->rowCount();
    // mengambil resource object dari query
    $res_rata2_populasi_negara = $db->query("SELECT AVG(population) rata2populasi 
        FROM country");   
    // melakukan fetch record sebagai object    
    $row_rata2_populasi_negara = $res_rata2_populasi_negara->fetchObject();     
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>World Database with PHP PDO</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
<?php 
    $menu = "home";
    include("parts/header.php"); 
?>
<h1>World Database</h1>
Jumlah negara: <?= $jml_country ?><br>
Jumlah kota: <?= $jml_city; ?><br>
Jumlah bahasa negara: <?= $jml_cl; ?><br>
Rata-rata Populasi Seluruh Negara: <?= $row_rata2_populasi_negara->rata2populasi ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>
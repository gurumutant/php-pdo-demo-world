# Instalasi di Laragon

- Pilih menu Quick app->Blank di Laragon, beri nama project (misal: **world-pdo**)
- Klik **Yes** saat ada permintaan konfirmasi dari User Account Control
- Buka terminal, lalu pindah ke folder sesuai nama project yang Anda pilih dengan perintah berikut, misal:<br>
`cd world-pdo` akhiri dengan <kbd>Enter</kbd>
- Clone project ini dengan perintah:<br>`git clone https://gitlab.com/gurumutant/php-pdo-demo-world.git ./`
- Lakukan penyesuaian pada config.php jika perlu. Diasumsikan bahwa pada komputer yang Anda gunakan sudah ada database **world** lengkap dengan datanya, yang diimport dari MySQL Sample Database
- Uji coba dengan mengakses URL project dari menu www di Laragon, atau secara langsung. Pada contoh ini, URL adalah `http://world-pdo.test`

# Melakukan Update 

Saat ada _commit_ baru di _repository_ ini, dan Anda tidak melakukan perubahan apapun pada project lokal Anda, cukup jalankan perintah ini di terminal (pastikan untuk dijalankan pada project folder):

>`git pull origin main`

Namun jika Anda sudah melakukan modifikasi sekecil apapun, jalankan terlebih dahulu perintah: 

>`git stash`

Baru jalankan perintah: 

>`git pull origin main`
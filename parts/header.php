<header class="d-flex justify-content-center py-3">
<ul class="nav nav-pills">
        <li class="nav-item"><a href="./" class="nav-link <?= ($menu=='home')?'active':''; ?>" aria-current="page">Home</a></li>
        <li class="nav-item"><a href="countries.php" class="nav-link <?= ($menu=='countries')?'active':''; ?>">Countries</a></li>
        <li class="nav-item"><a href="cities.php" class="nav-link <?= ($menu=='cities')?'active':''; ?>">Cities</a></li>
        <li class="nav-item"><a href="#" class="nav-link <?= ($menu=='countrylang')?'active':''; ?>">Country Languages</a></li>
        <li class="nav-item"><a href="bukutamu.php" class="nav-link <?= ($menu=='bukutamu')?'active':''; ?>">Buku Tamu</a></li>
      </ul>
</header>
<?php 
    require_once("config.php"); 

    // inisialisasi variabel
    $success = false; $errors = array();
    // mendeteksi ada pengiriman form apa tidak
    if (isset($_POST['submit'])) {
        // debugging pengiriman form
        // echo "<pre>"; print_r($_POST); echo "</prea>";

        // validasi form di sisi server
        if (trim($_POST['nama']) == "") {
            $errors[] = "Nama tidak boleh kosong!";
        }
        if (trim($_POST['email']) == "") {
            $errors[] = "Email tidak boleh kosong!";
        }
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Format email yang Anda gunakan tidak valid!";
        }
        if (trim($_POST['hp']) == "") {
            $errors[] = "Nomor HP tidak boleh kosong!";
        }
        if (trim($_POST['pesan']) == "") {
            $errors[] = "Pesan tidak boleh kosong!";
        }

        // cek apakah terdeteksi error
        if (count($errors) < 1) {
            $stm = $db->prepare('INSERT INTO bukutamu 
                                VALUES (?,?,?,?,?,NOW(),?)');
            $success = $stm->execute([
                NULL,
                $_POST['nama'],
                $_POST['email'],
                $_POST['hp'],
                $_POST['pesan'],
                $_SERVER['REMOTE_ADDR'],
            ]);  
        }                               
    }

    $res_bukutamu = $db->prepare("SELECT
                                    *
                                 FROM bukutamu
                                ");
    $res_bukutamu->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>World Database with PHP PDO</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <?php include("parts/additional_css.php"); ?>
</head>
<body>
<?php 
    $menu = "bukutamu";
    include("parts/header.php"); 
?>
<h1>Buku Tamu</h1>
<!-- jika variabel $success bernilai true, tampilkan pesan -->
<?php if($success) { ?>
    <div class="alert alert-success" role="alert">
        Terima kasih ! Isian buku tamu Anda berhasil disimpan.
    </div>
<?php } ?>
<!-- jika array $errors ada isinya, tampilkan daftar error -->
<?php if(count($errors) > 0) { ?>
    <div class="alert alert-danger" role="alert">
        <ul>
            <!-- perulangan untuk menampilkan isi array sebagai pesan error individual -->
            <?php foreach($errors as $e) { ?>
                <li><?= $e; ?></li>
            <?php } ?>    
        </ul>
    </div>
<?php } ?>
<div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Isi Buku Tamu</div>
                    <div class="card-body">
                        <form method="post">
                        <div class="row">
                            <label for="nama" class="col-md-4 col-form-label text-md-end">Nama</label>

                            <div class="col-md-8">
                                <input type="text" size="40" name="nama">
                            </div>
                        </div>
                        <div class="row">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>

                            <div class="col-md-6">
                                <input type="text" name="email">
                            </div>
                        </div>
                        <div class="row">
                            <label for="hp" class="col-md-4 col-form-label text-md-end">Nomor HP</label>

                            <div class="col-md-6">
                                <input type="text" name="hp">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label for="pesan" class="col-md-4 col-form-label text-md-end">Pesan</label>

                            <div class="col-md-6">
                                <textarea name="pesan" rows="5" cols="40"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
                            </div>
                        </div>
                        </form>    
                </div>
                </div>
            </div>
</div>                


<div class="row justify-content-center">
    <div class="col-md-8">
<table id="bukutamuTable" class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>e-Mail</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
<?php
    while ($row = $res_bukutamu->fetchObject()):
?>
    <tr>
        <td><?= $row->id ?></td>
        <td><?= $row->nama ?></td>
        <!-- TODO: menampilkan tanggal dalam format Indonesia -->
        <td><?= $row->tanggal ?></td>
        <td><?= $row->email ?></td>
        <td>
            <!-- TODO: menampilkan detail isian buku tamu menggunakan halaman terpisah atau modal window  -->
            <button type="button" class="btn btn-sm btn-primary">
                Detail
            </button>
        </td>
    </tr>
<?php endwhile; ?>   
    </tbody>    
</table>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<?php include("parts/additional_scripts.php"); ?>
<script>
    $(function(){	
        new DataTable('#bukutamuTable');    
    });
</script>
</body>
</html>
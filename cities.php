<?php 
    require_once("config.php"); 
    $res_city = $db->prepare("SELECT city.*, 
                              city.Name AS NamaKota, 
                              country.Name AS NamaNegara 
                              FROM city 
                              LEFT JOIN country 
                                ON city.CountryCode = country.Code");
    $res_city->execute();
    //$jml_city = $res_city->rowCount();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>World Database with PHP PDO</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <?php include("parts/additional_css.php"); ?>
</head>
<body>
<?php 
    $menu = "cities";
    include("parts/header.php"); 
?>
<h1>Cities of The World</h1>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
<table id="citiesTable" class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Kota</th>
            <th>Nama Negara</th>
            <th>Distrik</th>
            <th>Populasi</th>
        </tr>
    </thead>
    <tbody>
<?php
    while ($row = $res_city->fetchObject()):
?>
    <tr>
        <td><?= $row->ID ?></td>
        <td><?= $row->NamaKota ?></td>
        <td><?= $row->NamaNegara ?></td>
        <td><?= $row->District ?></td>
        <td><?= $row->Population ?></td>
    </tr>
<?php endwhile; ?>   
    </tbody>    
</table>
    </div>
    <div class="col-md-2"></div>
</div>

<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<?php include("parts/additional_scripts.php"); ?>
<script>
    $(function(){	
        new DataTable('#citiesTable');    
    });
</script>
</body>
</html>